@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class
)

package com.example.datastoragevibelab

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.datastoragevibelab.ui.data.Contact
import com.example.datastoragevibelab.ui.data.ContactsRepository
import com.example.datastoragevibelab.ui.data.DatabaseBuilder
import com.example.datastoragevibelab.ui.theme.DataStorageVIBELABTheme
import com.example.datastoragevibelab.ui.viewmodel.ContactsViewModel
import com.example.datastoragevibelab.ui.viewmodel.ContactsViewModelFactory


class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            DataStorageVIBELABTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    TotalScreen()
                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
        val viewModel: ContactsViewModel by viewModels(factoryProducer =
        {
            ContactsViewModelFactory(
                ContactsRepository(DatabaseBuilder.getInstance(this).contactDao()),
                this
            )
        })

        viewModel.checkDatabase(this)
    }
}

@Composable
fun AppBar(modifier: Modifier = Modifier) {
    CenterAlignedTopAppBar(
        title = {
            Text(
                text = "Заметки"
            )
        },
        modifier = modifier
    )
}

@Composable
fun Contact(
    contact: Contact,
    contactsViewModel: ContactsViewModel,
    modifier: Modifier = Modifier
) {
    val (showDialog, setShowDialog) = remember {
        mutableStateOf(false)
    }
    var expanded by remember { mutableStateOf(false) }
    Card(
        modifier = modifier,
        elevation = CardDefaults.cardElevation(10.dp),
        colors = CardDefaults.cardColors(MaterialTheme.colorScheme.primaryContainer)
    ) {
        Column(
            modifier = Modifier
                .padding(top = 16.dp, bottom = 16.dp)
                .fillMaxWidth()
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioNoBouncy,
                        stiffness = Spring.StiffnessMedium
                    )
                )
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = contact.nameContact,
                    fontSize = 20.sp,
                    modifier = Modifier
                        .padding(
                            bottom = 16.dp,
                            end = 54.dp
                        )
                        .align(Alignment.Bottom)
                )
                ExpandButton(expanded = expanded, onClick = { expanded = !expanded })
            }
            Text(
                text = contact.numberContact,
                fontSize = 12.sp,
                modifier = Modifier
                    .padding(
                        end = 6.dp
                    )
                    .align(Alignment.End)
            )
            if (expanded) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 6.dp)
                ) {
                    Text(
                        text = if (contact.comment != "") contact.comment else stringResource(R.string.empty_comment),
                        modifier = Modifier
                            .align(Alignment.CenterVertically),
                        fontStyle = FontStyle.Italic
                    )
                    AddDeleteButton(
                        onClickAdd = { setShowDialog(true) },
                        onClickDelete = {
                            contactsViewModel.updateContact(contact.copy(comment = "Пусто :("))
                        }
                    )
                }
            }
        }
    }
    NoteDialog(
        showDialog = showDialog,
        setShowDialog = setShowDialog,
        contact = contact,
        contactsViewModel = contactsViewModel
    )
}

@Composable
private fun ExpandButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(
        onClick = onClick
    ) {
        Icon(
            imageVector = if (expanded) Icons.Default.KeyboardArrowUp else Icons.Default.KeyboardArrowDown,
            contentDescription = null,
        )
    }
}

@Composable
private fun AddDeleteButton(
    onClickAdd: () -> Unit,
    onClickDelete: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
    ){
        IconButton(
            onClick = onClickAdd
        ) {
            Icon(
                imageVector = Icons.Default.Add,
                contentDescription = null
            )
        }
        IconButton(
            onClick = onClickDelete
        ) {
            Icon(
                imageVector = Icons.Default.Delete,
                contentDescription = null
            )
        }
    }
}

@Composable
fun NoteDialog(
    showDialog: Boolean,
    setShowDialog: (Boolean) -> Unit,
    contact: Contact,
    contactsViewModel: ContactsViewModel,
    modifier: Modifier = Modifier
) {
    if (showDialog) {
        var comment by remember { mutableStateOf("") }
        Dialog(
            onDismissRequest = { setShowDialog(false) }
        ) {
            Card {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = modifier
                ) {
                    Text(
                        text = stringResource(R.string.add_note),
                        modifier = Modifier
                            .padding(
                                bottom = 50.dp,
                                top = 16.dp
                            ),
                        fontSize = 24.sp
                    )
                    TextField(
                        value = comment,
                        onValueChange = { comment = it },
                        modifier = Modifier
                            .padding(
                                bottom = 68.dp,
                                start = 24.dp,
                                end = 24.dp
                            )
                    )
                    Button(
                        onClick = {
                            setShowDialog(false)
                            contactsViewModel.updateContact(contact.copy(comment = comment))
                                  },
                        modifier = Modifier
                            .padding(bottom = 16.dp)
                    ) {
                        Text(
                            text = stringResource(R.string.ready),
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun TotalScreen(
    modifier: Modifier = Modifier,
    contactsViewModel: ContactsViewModel = viewModel(
        factory =
        ContactsViewModelFactory(
            ContactsRepository(DatabaseBuilder.getInstance(LocalContext.current).contactDao()),
            LocalContext.current
        )
    )
){
    val contacts by contactsViewModel.contacts.collectAsState(initial = emptyList())
    Scaffold(
        topBar = {AppBar()}
    ) {
        LazyColumn(
            contentPadding = it,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            items(contacts) {contact ->
                Contact(
                    contact = contact,
                    contactsViewModel = contactsViewModel,
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth()
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    DataStorageVIBELABTheme {
        Contact(
            nameContact = "ZZZZZZZZZZZZZ",
            numberContact = "+3434242342424",
            comment = "Good person!"
        )
    }
}

@Preview(showSystemUi = true)
@Composable
fun Total() {
    DataStorageVIBELABTheme {
        TotalScreen()
    }
}

