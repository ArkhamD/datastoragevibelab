package com.example.datastoragevibelab.ui.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Contact::class], version = 1)
abstract class ContactsDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao
}

object DatabaseBuilder {

    private var INSTANCE: ContactsDatabase? = null

    fun getInstance(context: Context): ContactsDatabase {
        if (INSTANCE == null) {
            synchronized(ContactsDatabase::class) {
                if (INSTANCE == null) {
                    INSTANCE = buildRoomDB(context)
                }
            }
        }
        return INSTANCE!!
    }

    fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            ContactsDatabase::class.java,
            "contacts_database"
        ).build()

}