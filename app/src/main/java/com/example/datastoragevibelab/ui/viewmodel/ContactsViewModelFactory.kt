package com.example.datastoragevibelab.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.datastoragevibelab.ui.data.ContactsRepository

class ContactsViewModelFactory(
    private val contactsRepository: ContactsRepository,
    private val context: Context
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ContactsViewModel::class.java)) {
            return ContactsViewModel(contactsRepository, context) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}