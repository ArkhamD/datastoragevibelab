package com.example.datastoragevibelab.ui.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Contacts")
data class Contact(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val nameContact: String = "",
    val numberContact: String = "",
    val comment: String = ""
)
