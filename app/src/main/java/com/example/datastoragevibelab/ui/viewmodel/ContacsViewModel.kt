package com.example.datastoragevibelab.ui.viewmodel

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.datastoragevibelab.ui.data.Contact
import com.example.datastoragevibelab.ui.data.ContactsRepository
import kotlinx.coroutines.launch

class ContactsViewModel(
    private val contactRepository: ContactsRepository, context: Context
) : ViewModel() {

    var contacts = contactRepository.allContacts

    init {
        viewModelScope.launch {
            val contactsFromPhone = getAllContacts(context)
            val contactsFromDatabase = contacts

            if (contactsFromDatabase == null) {
                for (contact in contactsFromPhone) {
                    insertContact(contact)
                }
            }
        }
    }

    private fun insertContact(contact: Contact) {
        viewModelScope.launch {
            contactRepository.insertContact(contact)
        }
    }

    fun updateContact(contact: Contact) {
        viewModelScope.launch {
            contactRepository.updateContact(contact)
        }
    }

    private fun deleteContact(contact: Contact) {
        viewModelScope.launch {
            contactRepository.deleteContact(contact)
        }
    }

    fun checkDatabase(context: Context) {
        val contactsFromPhone = getAllContacts(context)
        val contactsFromDatabase = contacts
        viewModelScope.launch {
            contactsFromDatabase.collect{ contacts ->
                for (contact in contacts) {
                    if (contact.copy(comment = "") !in contactsFromPhone) {
                        deleteContact(contact)
                    }
                }
            }
        }
    }


    @SuppressLint("Range")
    private fun getAllContacts(context: Context): List<Contact> {
        val contacts: MutableList<Contact> = mutableListOf()

        val contentResolver: ContentResolver = context.contentResolver
        val cursor: Cursor? =
            contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        cursor?.let {
            if (it.count > 0) {
                while (it.moveToNext()) {
                    val name: String =
                        it.getString(it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                    var phoneNumber = ""
                    if (it.getInt(it.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        val phoneCursor: Cursor? = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(it.getString(it.getColumnIndex(ContactsContract.Contacts._ID))),
                            null
                        )
                        phoneCursor?.let { pc ->
                            if (pc.moveToNext()) {
                                phoneNumber =
                                    pc.getString(pc.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            }
                            pc.close()
                        }
                    }
                    val contact = Contact(
                        id = it.getInt(it.getColumnIndex(ContactsContract.Contacts._ID)).toLong(),
                        nameContact = name,
                        numberContact = phoneNumber,
                        comment = ""
                    )

                    contacts.add(contact)
                }
            }
            it.close()
        }

        return contacts
    }

}